import {NgModule} from '@angular/core';
import {
  NzBreadCrumbModule,
  NzButtonModule,
  NzGridModule,
  NzLayoutModule,
  NzMenuModule, NzMessageModule, NzProgressModule,
  NzStepsModule,
  NzToolTipModule
} from 'ng-zorro-antd';
import {IconsProviderModule} from './icons-provider.module';
import {NzSpaceModule} from 'ng-zorro-antd/space';

@NgModule({
  imports: [
    IconsProviderModule,
    NzGridModule,
    NzLayoutModule,
    NzMenuModule,
    NzButtonModule,
    NzToolTipModule,
    NzSpaceModule,
    NzBreadCrumbModule,
    NzStepsModule,
    NzProgressModule,
    NzMessageModule
  ],
  exports: [
    IconsProviderModule,
    NzGridModule,
    NzLayoutModule,
    NzMenuModule,
    NzButtonModule,
    NzToolTipModule,
    NzSpaceModule,
    NzBreadCrumbModule,
    NzStepsModule,
    NzProgressModule,
    NzMessageModule
  ]
})
export class NzWrapperModule {}
