import {Inject, Injectable} from '@angular/core';
import {DOCUMENT} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  constructor(@Inject(DOCUMENT) private document: Document) { }

  switchTheme(themeName: string | null) {
    const themeFilePath = `/assets/themes/${themeName}.css`;
    localStorage.setItem('theme', themeName || '');
    const headEl = this.document.getElementsByTagName('head')[0];

    const currentThemeLinkEl = this.document.getElementById('theme-link') as HTMLLinkElement;
    if (currentThemeLinkEl) {
      currentThemeLinkEl.href = themeFilePath;
      return;
    }

    const newThemeLinkEl = this.document.createElement('link');
    newThemeLinkEl.id = 'theme-link';
    newThemeLinkEl.rel = 'stylesheet';
    newThemeLinkEl.href = themeFilePath;
    headEl.appendChild(newThemeLinkEl);
  }
}
