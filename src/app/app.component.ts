import { Component, OnInit } from '@angular/core';
import { ThemeService } from './services/theme/theme.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
})
export class AppComponent implements OnInit {
  isCollapsed = false;
  constructor(private themeService: ThemeService) {}
  ngOnInit(): void {
    if (localStorage.getItem('theme')) {
      this.themeService.switchTheme(localStorage.getItem('theme'));
    }
  }
}
