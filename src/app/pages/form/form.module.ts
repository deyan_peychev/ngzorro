import {NgModule} from '@angular/core';
import {BasicComponent} from './basic/basic.component';
import {CommonModule} from '@angular/common';
import {FormRoutingModule} from './form-routing.module';

@NgModule({
  declarations: [
    BasicComponent
  ],
  imports: [
    CommonModule, FormRoutingModule
  ]
})
export class FormModule {}
