import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BasicComponent} from './basic/basic.component';

const formRoutes: Routes = [
  { path: '', children: [
      { path: '', redirectTo: '/form/basic' },
      { path: 'basic', component: BasicComponent, data: { breadcrumb: 'Basic' }  }
    ]}
  ];

@NgModule({
  imports: [RouterModule.forChild(formRoutes)],
  exports: [RouterModule],
})
export class FormRoutingModule {}
