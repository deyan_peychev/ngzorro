import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WelcomeComponent} from './welcome/welcome.component';
import {MonitorComponent} from './monitor/monitor.component';
import {WorkplaceComponent} from './workplace/workplace.component';

const dashboardRoutes: Routes = [
  { path: '', children: [
      { path: '', redirectTo: '/dashboard/welcome', pathMatch: 'full'},
      { path: 'welcome', component: WelcomeComponent, data: { breadcrumb: 'Welcome' } },
      { path: 'monitor', component: MonitorComponent, data: { breadcrumb: 'Monitor' }  },
      { path: 'workplace', component: WorkplaceComponent, data: { breadcrumb: 'Workplace' }  },
  ]}
];

@NgModule({
  imports: [
    RouterModule.forChild(dashboardRoutes)
  ],
  exports: [RouterModule]
})
export class DashboardRoutingModule {}
