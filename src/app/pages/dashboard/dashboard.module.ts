import {NgModule} from '@angular/core';
import {WelcomeComponent} from './welcome/welcome.component';
import {CommonModule} from '@angular/common';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {MonitorComponent} from './monitor/monitor.component';
import {WorkplaceComponent} from './workplace/workplace.component';
import {NzWrapperModule} from '../../nz-wrapper.module';

@NgModule({
  declarations: [
    WelcomeComponent,
    MonitorComponent,
    WorkplaceComponent
  ],
  imports: [
    CommonModule, NzWrapperModule, DashboardRoutingModule
  ]
})
export class DashboardModule {}
