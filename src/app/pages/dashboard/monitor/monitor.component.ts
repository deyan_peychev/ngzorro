import { Component, OnInit } from '@angular/core';

interface ProgressBar {
  percentage: number;
}

@Component({
  selector: 'app-monitor',
  templateUrl: './monitor.component.html',
  styleUrls: ['./monitor.component.less'],
})
export class MonitorComponent implements OnInit {
  currentStep: number;
  maxSteps: number[];
  progressBars: ProgressBar[];
  constructor() {}
  ngOnInit(): void {
    this.loadStepsData(10);
    this.loadProgressLineData(3);
  }
  loadStepsData(size: number): void {
    this.maxSteps = Array(size)
      .fill(0)
      .map((el: number, index: number) => index);
    this.currentStep = Math.floor(Math.random() * Math.floor(size)) + 1;
  }
  loadProgressLineData(size: number): void {
    this.progressBars = Array(size)
      .fill(null)
      .map((el: ProgressBar) => {
        return {
          percentage: Math.floor(Math.random() * Math.floor(100)) + 1,
        };
      });
  }
}
