import {Component} from '@angular/core';
import {ThemeService} from '../../../services/theme/theme.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './workplace.component.html',
  styleUrls: ['./workplace.component.less']
})
export class WorkplaceComponent {

  constructor(private themeService: ThemeService) { }

  switchTheme(themeFile: string) {
    this.themeService.switchTheme(themeFile);
  }
}
