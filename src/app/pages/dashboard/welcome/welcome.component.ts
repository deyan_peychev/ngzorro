import {Component} from '@angular/core';
import {NzMessageService} from 'ng-zorro-antd';
import {concatMap} from 'rxjs/operators';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.less'],
})
export class WelcomeComponent {
  nzMessageSubscription: Subscription;
  constructor(private nzMessageService: NzMessageService) {}

  eventButtonOnClick() {
    console.log('Button pressed!');
    alert('Button pressed');
  }
  showErrorMessage() {
    this.nzMessageSubscription = this.nzMessageService.loading('Loading error message...', {nzDuration: 2000})
      .onClose.pipe(
        concatMap(() => this.nzMessageService.error('Error message loaded.', {nzDuration: 2000}).onClose)
    ).subscribe(() => {
      this.nzMessageService.info('Message service subscription has passed.');
      this.nzMessageSubscription.unsubscribe();
    });
  }
}
